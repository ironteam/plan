import Calendar from '@models/calendar.model'
import Event from '@models/event.model'
import { required, integer } from 'vuelidate/lib/validators'
import { datetimeValidator } from '@helpers/custom-validators'

export default {
  name: 'QuickEditEvent',
  props: {
    calendarEvent: {
      type: Object,
      required: true
    }
  },
  validations: function () {
    return {
      storeEvent: {
        title: {
          required
        },
        calendar_id: {
          required,
          integer
        }
      },
      formEvent: {
        start: {
          required,
          datetimeValidator
        },
        end: {
          required,
          datetimeValidator
        }
      }
    }
  },
  data () {
    return {
      formEvent: {},
      isDeleted: false
    }
  },
  // Transfert de la prop source dans le formualaire
  beforeMount: function () {
    this.formEvent = {
      title: this.calendarEvent.title || '',
      is_all_day: this.calendarEvent.is_all_day,
      calendar: this.calendars.filter(option => option.value === this.calendarEvent.calendar_id)[0],
      start: this.$moment(this.calendarEvent.start).format(this.displayDateFormat),
      end: this.$moment(this.calendarEvent.end).format(this.displayDateFormat)
    }
  },
  methods: {
    create: function () {
      Event.$create({ data: this.storeEvent })
        .then(() => this.$store.dispatch('alert/success', 'Enregistrement réussi'))
        .then(() => this.$emit('close'))
    },
    update: function () {
      Event.$update({
        params: { id: this.calendarEvent.id },
        data: this.storeEvent })
        .then(() => this.$store.dispatch('alert/success', 'Enregistrement réussi'))
        .catch((response) => this.$store.dispatch('alert/error', response.data.message))
        .then(() => this.$emit('close'))
    },
    remove: function () {
      Event.$delete({ params: { id: this.calendarEvent.id } })
        .then(this.isDeleted = true)
        .then(() => this.$store.dispatch('alert/success', 'Suppression réussie'))
        .then(() => this.$emit('close'))
    },
    // Paramètres pour les champs date et time
    startFieldOptions: function (startDate) {
      return this.$moment(this.formEvent.end, this.displayDateFormat)
        .diff(this.$moment(startDate, 'YYYY/MM/DD'), 'minutes') > 1
    },
    startTimeFieldOptions: function (hour, minute) {
      return this.$moment(this.formEvent.end, this.displayDateFormat)
        .diff(this.$moment(this.formEvent.start, this.displayDateFormat)
          .set('hour', hour).set('minute', minute || 59), 'minutes') > 1
    },
    endFieldOptions: function (date) {
      return this.$moment(date, 'YYYY/MM/DD')
        .diff(this.$moment(this.formEvent.start, this.displayDateFormat), 'minutes') >= 0
    },
    endTimeFieldOptions: function (hour, minute) {
      return this.$moment(this.formEvent.end, this.displayDateFormat)
        .set('hour', hour).set('minute', minute || 59)
        .diff(this.$moment(this.formEvent.start, this.displayDateFormat), 'minutes') > 0
    }
  },
  computed: {
    isNew: (self) => self.calendarEvent.id === null,
    currentEvent: (self) => Event.find(self.calendarEvent.id),
    calendars: (self) => Calendar.all().map(function (calendar) {
      return {
        label: calendar.name,
        value: calendar.id,
        icon: 'fas fa-folder',
        color: calendar.colorName
      }
    }),
    displayDateFormat: (self) => (self.formEvent.is_all_day ? 'DD/MM/YYYY' : 'DD/MM/YYYY HH:mm'),
    isCreating: (self) => self.$store.getters['entities/events/loading'],
    isUpdating: (self) => (self.currentEvent && self.currentEvent.$isUpdating === true),
    isDeleting: (self) => (self.currentEvent && self.currentEvent.$isDeleting === true),
    // Mise à jour de l'objet destiné à la mutation du store
    storeEvent: function () {
      const event = {
        calendar_id: this.formEvent.calendar.value,
        title: this.formEvent.title,
        is_all_day: this.formEvent.is_all_day,
        start: this.$moment(this.formEvent.start, this.displayDateFormat),
        end: this.$moment(this.formEvent.end, this.displayDateFormat)
      }

      // Si toute le journée
      if (event.is_all_day) {
        event.start = event.start.hour(0).minute(0)
        event.end = event.start.clone().add(1, 'days').hour(0).minute(0)
      }

      // Forcer le formualire à rafraichir la date de départ
      this.formEvent.start = event.start.format(this.displayDateFormat)

      return {
        ...event,
        start: event.start.format(),
        end: event.end.format()
      }
    }
  }
}
