import { Model } from '@vuex-orm/core'
import Event from './event.model'

export default class Calendar extends Model {
  // This is the name used as module name of the Vuex Store.
  static entity = 'calendars'
  static primaryKey = 'id'

  // List of all fields (schema) of the post model. `this.attr` is used
  // for the generic field type. The argument is the default value.
  static fields () {
    return {
      id: this.number(0),
      name: this.string('Calendrier'),
      // color_id: this.number(0), // this.belongsTo(Color, 'color_id')
      backgroundColor: this.string('#B0B0B0'),
      borderColor: this.string('#B0B0B0'),
      colorName: this.string('dark-grey'),
      is_visible: this.boolean(true),
      events: this.hasMany(Event, 'calendar_id')
    }
  }
}
