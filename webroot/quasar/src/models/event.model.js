import { Model } from '@vuex-orm/core'
// import Calendar from './calendar.model'

export default class Event extends Model {
  // This is the name used as module name of the Vuex Store.
  static entity = 'events'
  static primaryKey = 'id'

  // List of all fields (schema) of the post model. `this.attr` is used
  // for the generic field type. The argument is the default value.
  static fields () {
    return {
      id: this.number(null),
      title: this.string('Nouvel évènement'),
      calendar_id: this.number(1),
      start: this.string('2019-05-13 08:00:00'),
      end: this.string('2019-05-13 12:00:00'),
      is_all_day: this.boolean(false),
      classNames: this.attr(['cursor-pointer']),
      droppable: this.boolean(true),
      editable: this.boolean(true)
    }
  }
}
