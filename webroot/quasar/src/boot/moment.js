import * as moment from 'moment'

export default async ({ app, Vue }) => {
  Vue.prototype.$moment = moment
  Vue.prototype.$moment.locale(app.i18n.locale)
  Vue.prototype.$moment.defaultFormat = 'YYYY-MM-DDTHH:mm:ssZ'
}
