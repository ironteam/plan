import axios from 'axios'
import { parseCakeResponse, parseCakeError } from '@helpers'

export default async ({ Vue }) => {
  Vue.prototype.$axios = axios
  Vue.prototype.$axios.defaults.baseURL = process.env.API_BASE_URL
  Vue.prototype.$axios.interceptors.response.use(parseCakeResponse, parseCakeError)
}
