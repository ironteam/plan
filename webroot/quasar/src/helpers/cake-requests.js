import { userService } from '@services'

export function parseCakeResponse (response) {
  return response.data
}

export function parseCakeError (error) {
  const unauthorized = error.response.status === 401 &&
    error.response.data.data.url !== '/users/login.json'
  const expired = error.response.status === 500 &&
    error.response.data.data.message === 'Expired token'

  // Un code 401 indique que notre session n'est plus valide
  if (unauthorized || expired) {
    userService.logout()
    window.location.reload(true)
  }

  return Promise.reject(error.response.data)
}
