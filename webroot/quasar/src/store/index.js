import Vue from 'vue'
import Vuex from 'vuex'
import VuexORM from '@vuex-orm/core'
import VuexORMAxios from '@vuex-orm/plugin-axios'

import { userService } from '@services'

import Calendar from '@models/calendar.model'
import Event from '@models/event.model'

import alert from './alert'
import authentication from './authentication'
import layout from './layout'

Vue.use(Vuex)

/**
 * VueORM Settings
 */

// Create a new database instance.
const database = new VuexORM.Database()

// Register Models to the database.
database.register(Calendar)
database.register(Event)

// Axios ORM Plugin
VuexORM.use(VuexORMAxios, {
  database,
  http: {
    baseURL: process.env.API_BASE_URL,
    url: '/',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    access_token: () => localStorage.getItem('token'),
    onResponse: (response) => response.data.data,
    onError: function (error) {
      if (error.response.data.data.message === 'Expired token') {
        userService.logout()
        window.location.reload(true)
      }

      return Promise.reject(error.response.data)
    }
  }
})

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    plugins: [VuexORM.install(database)],
    modules: {
      alert,
      authentication,
      layout
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV
  })

  return Store
}
