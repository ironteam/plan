export function openSidebar ({ commit }) {
  commit('switchSidebar', true)
}

export function closeSidebar ({ commit }) {
  commit('switchSidebar', false)
}
