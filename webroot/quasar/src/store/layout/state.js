import { Platform } from 'quasar'

export default {
  sidebar: Platform.is.desktop
}
