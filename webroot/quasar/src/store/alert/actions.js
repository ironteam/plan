import { Notify } from 'quasar'

export function success ({ commit, state }, message) {
  commit('success', message)
  Notify.create({
    type: 'positive',
    message,
    position: 'bottom',
    timeout: state.timeout,
    icon: 'far fa-check-circle',
    closeBtn: 'Ok',
    color: 'positive',
    textColor: 'white'
  })
}

export function error ({ commit, state }, message) {
  commit('error', message)
  Notify.create({
    message,
    position: 'bottom',
    timeout: state.timeout,
    icon: 'far fa-times-circle',
    closeBtn: 'Ok',
    color: 'negative',
    textColor: 'white'
  })
}

export function errors ({ commit, dispatch }, errors) {
  commit('errors', errors)

  // Décomposition de toutes les erreurs de validation
  Object.entries(errors).forEach(function (fieldErrors) {
    Object.entries(fieldErrors[1]).forEach(function (validatorError) {
      dispatch('alert/error', fieldErrors[0] + ' : ' + validatorError[1], { root: true })
    })
  })
}

export function clear ({ commit }) {
  commit('clear', null)
}
