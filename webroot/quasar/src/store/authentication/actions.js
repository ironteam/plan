import { userService } from '@services'

export async function login ({ dispatch, commit }, { email, password }) {
  commit('loginRequest', { email })

  return userService
    .login(email, password)
    .then(
      data => {
        commit('loginSuccess', data.user)
        dispatch('alert/success', 'Bienvenue, vous êtes connecté', { root: true })
        this.$router.push({ path: '/' })

        return data
      },
      error => {
        commit('loginFailure', error)
        dispatch('alert/error', error.data.message, { root: true })

        return error
      }
    )
}

export function logout ({ dispatch, commit }) {
  userService.logout()
  commit('logout')
  dispatch('alert/success', 'Vous êtes déconnecté de votre compte', { root: true })
  this.$router.push({ path: '/login' })
}
