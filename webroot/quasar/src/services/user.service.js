import axios from 'axios'
import { authHeaders } from '@helpers'

export const userService = {
  login,
  logout,
  get
}

function login (email, password) {
  return axios
    .post(`/users/login.json`, { email, password })
    .then(dataObject => {
      if (dataObject.data.token) {
        // store jwt token in local storage to keep user logged in between page refreshes
        localStorage.setItem('token', dataObject.data.token)
        localStorage.setItem('user', JSON.stringify(dataObject.data.user))
      }

      return dataObject
    })
}

function logout () {
  // remove user from local storage to log user out
  localStorage.removeItem('token')
  localStorage.removeItem('user')
}

function get (userId) {
  return axios.get(`/users/${userId}.json`, { headers: authHeaders() })
}
