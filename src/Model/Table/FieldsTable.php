<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Fields Model
 *
 * @property \App\Model\Table\FormsTable|\Cake\ORM\Association\BelongsTo $Forms
 * @property \App\Model\Table\AttrsTable|\Cake\ORM\Association\BelongsTo $Attrs
 *
 * @method \App\Model\Entity\Field get($primaryKey, $options = [])
 * @method \App\Model\Entity\Field newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Field[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Field|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Field saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Field patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Field[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Field findOrCreate($search, callable $callback = null, $options = [])
 */
class FieldsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('fields');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsTo('Forms', [
            'foreignKey' => 'form_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Attrs', [
            'foreignKey' => 'attr_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 50)
            ->requirePresence('name', 'create')
            ->allowEmptyString('name', false);

        $validator
            ->scalar('label')
            ->maxLength('label', 255)
            ->allowEmptyString('label');

        $validator
            ->scalar('type')
            ->maxLength('type', 50)
            ->requirePresence('type', 'create')
            ->allowEmptyString('type', false);

        $validator
            ->scalar('default')
            ->maxLength('default', 255)
            ->allowEmptyString('default');

        $validator
            ->scalar('placeholder')
            ->maxLength('placeholder', 255)
            ->allowEmptyString('placeholder');

        $validator
            ->scalar('attr_class')
            ->maxLength('attr_class', 50)
            ->allowEmptyString('attr_class');

        $validator
            ->boolean('disabled')
            ->requirePresence('disabled', 'create')
            ->allowEmptyString('disabled', false);

        $validator
            ->integer('max')
            ->allowEmptyString('max');

        $validator
            ->allowEmptyString('maxlength');

        $validator
            ->integer('min')
            ->allowEmptyString('min');

        $validator
            ->scalar('pattern')
            ->maxLength('pattern', 255)
            ->allowEmptyString('pattern');

        $validator
            ->boolean('readonly')
            ->requirePresence('readonly', 'create')
            ->allowEmptyString('readonly', false);

        $validator
            ->boolean('required')
            ->requirePresence('required', 'create')
            ->allowEmptyString('required', false);

        $validator
            ->allowEmptyString('size');

        $validator
            ->integer('step')
            ->allowEmptyString('step');

        $validator
            ->requirePresence('rows', 'create')
            ->allowEmptyString('rows', false);

        $validator
            ->allowEmptyString('cols');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['form_id'], 'Forms'));
        $rules->add($rules->existsIn(['attr_id'], 'Attrs'));

        return $rules;
    }
}
