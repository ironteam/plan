<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Field Entity
 *
 * @property int $id
 * @property int $form_id
 * @property string $name
 * @property string|null $label
 * @property string $type
 * @property string|null $default
 * @property string|null $placeholder
 * @property string|null $attr_id
 * @property string|null $attr_class
 * @property bool $disabled
 * @property int|null $max
 * @property int|null $maxlength
 * @property int|null $min
 * @property string|null $pattern
 * @property bool $readonly
 * @property bool $required
 * @property int|null $size
 * @property int|null $step
 * @property int $rows
 * @property int|null $cols
 *
 * @property \App\Model\Entity\Form $form
 * @property \App\Model\Entity\Attr $attr
 */
class Field extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'form_id' => true,
        'name' => true,
        'label' => true,
        'type' => true,
        'default' => true,
        'placeholder' => true,
        'attr_id' => true,
        'attr_class' => true,
        'disabled' => true,
        'max' => true,
        'maxlength' => true,
        'min' => true,
        'pattern' => true,
        'readonly' => true,
        'required' => true,
        'size' => true,
        'step' => true,
        'rows' => true,
        'cols' => true,
        'form' => true,
        'attr' => true
    ];
}
