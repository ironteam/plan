<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Calendar Entity
 *
 * @property int $id
 * @property string $name
 * @property string $color
 *
 * @property \App\Model\Entity\Event[] $events
 */
class Calendar extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'color' => true,
        'events' => true
    ];

    protected $_hidden = [
        'color'
    ];

    protected $_virtual = ['backgroundColor', 'borderColor', 'colorName'];

    protected function _getBackgroundColor()
    {
        return $this->color->code;
    }

    protected function _getBorderColor()
    {
        return $this->color->code;
    }

    protected function _getColorName()
    {
        return $this->color ? $this->color->name : 'primary';
    }
}
