<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Event Entity
 *
 * @property int $id
 * @property int $calendar_id
 * @property string $title
 * @property \Cake\I18n\FrozenTime $start
 * @property \Cake\I18n\FrozenTime|null $end
 * @property string $repeat_frequency
 * @property int $repeat_count
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Calendar $calendar
 */
class Event extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'calendar_id' => true,
        'title' => true,
        'start' => true,
        'end' => true,
        'is_all_day' => true,
        'repeat_frequency' => true,
        'repeat_count' => true,
        'created' => true,
        'modified' => true,
        'calendar' => true
    ];

    protected $_virtual = ['class_names'];

    protected function _getClassNames()
    {
        return ['cursor-pointer'];
    }
}
