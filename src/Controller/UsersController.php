<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
//use Cake\Auth\DefaultPasswordHasher;
use Cake\Http\Exception\UnauthorizedException;
use Cake\Utility\Security;
use Firebase\JWT\JWT;

class UsersController extends AppController
{
    /**
     * Initialisation du controlleur
     */
    public function initialize()
    {
        parent::initialize();

        // La route login est accessible sans authentification
        $this->Auth->allow(['login']);
        // On ne vérifie pas les autorisations pour la route login
        $this->Authorization->skipAuthorization(['login']);
    }

    /**
     * Connexion manuelle
     *
     * @param string $username Identifiant utilisateur
     * @param string $password Mot de passe associé
     *
     * @return json
     */
    /**
     * @SWG\Post(
     *     path="/authentication/users/login",
     *     summary="Obtention d'un token lié à un utilisateur",
     *     tags={"Authentication"},
     *     description="Connexion à l'api qui permet d'obtenir un token pour authentifier les futures requêtes.
     Le token est à placer dans le header des requêtes dans un champs 'Authorization' et sa valeur doit être sous la forme : 'Bearer TOKEN'
     ",
     *     operationId="loginUser",
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Objet Companies à ajouter",
     *         required=true,
     *         @SWG\Schema(
     *             @SWG\Property(property="username", format="string", maxLength=50, example="demo"),
     *             @SWG\Property(property="password", format="string", maxLength=50, example="123456")
     *         )
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Résultat du login",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="success", format="boolean", default=true, example=true),
     *             @SWG\Property(
     *                 property="data",
     *                 type="object",
     *                  @SWG\Property(property="id", format="int64", example="1"),
     *                  @SWG\Property(property="token", format="string", maxLength=255, example="Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c")
     *             )
     *         )
     *     ),
     *     @SWG\Response(response="400", ref="#/responses/error400"),
     *     @SWG\Response(response="500", ref="#/responses/error500")
     * )
     */
    public function login()
    {
        //$hasher = new DefaultPasswordHasher();
        //print_r($hasher->hash('123456'));exit;

        if ($this->request->is('post')) {
            // Check du login
            if (!($user = $this->Auth->identify())) {
                throw new UnauthorizedException(__("Invalid account username or password"));
            }

            $this->set([
                'success' => true,
                'data' => [
                    'token' => $this->_generateJwt($user['id']),
                    'user' => $user
                ]
            ]);
        } else {
            $this->set(['success' => false, 'data' => []]);
        }

        $this->set(['_serialize' => ['success', 'data']]);
    }

    /**
     * Génération du jeton JWT pour l'authentification
     *
     * @param  integer $user_id    ID de l'utilisateur
     * @param  integer $expiration Durée d'expiration choisi
     *
     * @return string   Jeton JWT
     */
    private function _generateJwt($user_id, $expiration = 604800)
    {
        return JWT::encode(
            [ 'sub' => $user_id, 'exp' =>  time() + $expiration ],
            Security::getSalt()
        );
    }

    /**
     * Visualisation de son compte utilisateur courant
     *
     * @return void
     */
    public function me()
    {
        $this->set([
            'success' => true,
            'data' => $this->Auth->user(),
            '_serialize' => ['success', 'data']
        ]);
    }
}
