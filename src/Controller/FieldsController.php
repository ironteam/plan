<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Fields Controller
 *
 * @property \App\Model\Table\FieldsTable $Fields
 *
 * @method \App\Model\Entity\Field[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FieldsController extends AppController
{
	public function index()
    {
        //Filtre du formulaire
        $this->Crud->on('beforePaginate', function (\Cake\Event\Event $event) 
        {
            //Récuperation du form_id pour les routes forms/ID/fields
            $form_id = $this->request->getParam('form_id');
            //Ajout de la condition si l'id est présent
            if ($form_id) {
                $this->paginate['conditions']['Fields.form_id'] = $form_id;
            }
        });

        return $this->Crud->execute();
    }
}
