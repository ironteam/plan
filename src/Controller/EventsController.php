<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Fields Controller
 *
 * @property \App\Model\Table\EventsTable $Fields
 *
 * @method \App\Model\Entity\Event[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class EventsController extends AppController
{
    public function beforeFilter(\Cake\Event\Event $event)
    {
        parent::beforeFilter($event);

        $this->Authorization->authorizeModel('index');
    }

    /**
     * Listing des évènements
     */
    public function index()
    {
        // Filtre du formulaire
        $this->Crud->on('beforePaginate', function (\Cake\Event\Event $event) {
            // Encadrement des dates
            $this->paginate['conditions']['DATE(Events.start) >='] = $this->request->getQuery('from');
            $this->paginate['conditions']['DATE(Events.end) <'] = $this->request->getQuery('until');
            // Récuperation du form_id pour les routes forms/ID/fields
            $calendar_id = $this->request->getQuery('calendar_id');
            // Ajout de la condition si l'id est présent
            if ($calendar_id) {
                $this->paginate['conditions']['Events.calendar_id'] = $calendar_id;
            }
        });

        return parent::index();
    }
}
