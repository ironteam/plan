<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Calendars Controller
 *
 * @property \App\Model\Table\CalendarsTable $Fields
 *
 * @method \App\Model\Entity\Event[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CalendarsController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        $this->Crud->listener('relatedModels')->relatedModels([
          'Colors',
          //'Events'
        ]);

        //$this->Authorization->authorizeModel('index');
    }
}
